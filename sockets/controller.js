const socketController =  ( socket ) => {
    console.log('Client connected' + socket.id);

    socket.on( 'disconnect', () => {
        console.log( 'Client disconnected' );
    } );
  
    // receive a message from the client
    socket.on( "sent-message", ( payload, callback ) => {
        const id = 1234123;
        callback( id );

        socket.broadcast.emit( 'sent-message', payload );
    });
}

module.exports = {
    socketController
}