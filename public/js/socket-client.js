const lblOnline = document.getElementById("lblOnline");
const lblOffline = document.getElementById("lblOffline");
const textMensage = document.getElementById("textMensage");
const btnSend= document.getElementById("btnSend");

const socket = io();

socket.on( 'connect', () => {
    // console.log( 'Connected' );

    lblOffline.style.display = 'none';
    lblOnline.style.display = '';
} );

socket.on( 'disconnect', () => {
    // console.log( 'Disconnected' );

    lblOffline.style.display = '';
    lblOnline.style.display = 'none';
} );

socket.on( 'sent-message', ( payload ) => {
    console.log(payload);
});

btnSend.addEventListener( 'click', () => {
    const message = textMensage.value;
    const payload = {
        message,
        id: '123ABC',
        date: new Date().getTime()
    };

    socket.emit( 'sent-message', payload, ( id ) => {
        console.log( 'From server', id );
    });
    
} );